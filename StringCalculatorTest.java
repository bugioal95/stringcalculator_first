import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class StringCalculatorTest {
	private StringUtility stringCalculatorInstance;
	@Before
	public void setUp() throws Exception {
		stringCalculatorInstance = new StringUtility();
	}

	@Test
	public void testEmptyString() {
		assertEquals(stringCalculatorInstance.add(""), 0);
	}
	@Test
	public void testOneNumber(){
		assertEquals(1,stringCalculatorInstance.add("1"));
	}
	@Test
	public void testTwoNumber(){
		assertEquals(3,stringCalculatorInstance.add("1,2"));
	}
	@Test
	public void testWithNewLine(){
		assertEquals(3,stringCalculatorInstance.add("1\n2"));
	}
	@Test
	public void testWithDifferentDelimiters(){
		assertEquals(3,stringCalculatorInstance.add("//;\n1;2"));
	}
	@Test
	public void testIgnoreNumbersBiggestThatMiles(){
		assertEquals(1,stringCalculatorInstance.add("1,1001"));
	}
	@Test
	public void testDelimitersWithAnyOccurrences(){
		assertEquals(6,stringCalculatorInstance.add("//[***]\n1***2***3"));
	}
	@Test
	public void testAllCombination(){
		assertEquals(6, stringCalculatorInstance.add("//[*][%]\n1*2%3"));
	}
}
